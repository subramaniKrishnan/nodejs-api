var mongoose = require('mongoose');
var randtoken = require('rand-token');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var UsersSchema = new Schema({

    name: {
        type: String,
        unique: true
    },
    email: { type: String, required: true, unique: true },
    password:{
          type:String,
          required:true
    },
    phone: { type: Number },
    isVerified: Boolean,
    token: {
        type: Schema.Types.ObjectId,
        ref: 'Token',
        default: null
    }
});
var TokenSchema = new Schema({
    value: {
        type: String,
        unique: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "Users"
    },
    expireAt: {
        type: Date,
        expires: 60,
        default: Date.now
    },
    createdAt:{
        type: Date,
        default: Date.now
    }
});

var Token = mongoose.model('Token', TokenSchema);

UsersSchema.methods.generateToken = function () {

    var moken = new Token();
    moken.value = randtoken.generate(32);
    moken.user = this._id;
    this.token = moken._id;
    this.save(function (err) {

        if (err)
            throw err

            moken.save(function (err) {
            if (err)
                throw err;
        });

    })

};

UsersSchema.methods.generateHash =function(password){

    return bcrypt.hashSync(password, bcrypt.genSaltSync(9));

};

UsersSchema.methods.validPassword  =function(password){
    
    return bcrypt.compareSync(password, this.password);
    
    };
   
    var User =mongoose.model("Users", UsersSchema);
    var Models = { User: User, Token: Token };

module.exports = Models;