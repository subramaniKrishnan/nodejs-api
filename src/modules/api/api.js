'use strict';
var express = require('express');
var api = express.Router();
var passport = require("passport");



//import controllers 
var user = require('./controllers/userController');


//before action begin
//api.use(passport.authenticate('bearer', { session: false }));
api.use((req, res, next) => {

    console.log("Before Action..");
    next();
})

api.use("/user",user);

//If Request does't match call this function
api.use((req, res, next) => {
    res.status(404).json({ message: "Page not found" });
})
module.exports = api;