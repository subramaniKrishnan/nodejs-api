'use strict';
var express = require('express');
var user = express.Router();

var UserModal = require('./../modals/userModal').User;

user.use(function (req, res, next) {

    //console.log("before action");
    next();

});

user.get('/', function (req, res) {


    UserModal.find(function (err, users) {

        if (err) {
            res.json(err);
        } else {

            res.json(users);

        }

    });

});

//create new users

user.post('/add', function (req, res) {

    const data = req.body;
    // console.log("Body Payloads",req.body);
    // console.log("Query Params",req.query);

    var userModal = new UserModal(data);

    userModal.save(function (err, muser) {

        if (err) {
            res.json(err);

        } else {

            res.json({ message: 'New user created successfuly', data: muser });
        }


    });

    //res.json({ message: 'Post Action happend' }); 


});

user.get('/list', function (req, res) {

    UserModal.find(function (err, users) {

        if (err) {
            res.json(err);
        } else {

            res.json(users);

        }

    });
});

user.delete("/delete", function (req, res) {

    //  console.log("Delete Requer",req.query.id);
    UserModal.remove({ _id: req.query.id }, (err, user) => {

        if (err) {
            res.status(422).send(err);
        }

        res.status(200).json({ message: "User successfully deleted" });
    })

})

user.put("/update", function (req, res) {

    const updateData = req.body;
    UserModal.findOneAndUpdate({ _id: req.query.id }, updateData, (err, user) => {

        if (err) {
            res.status(422).send(err);
        }

        res.status(200).json({ message: "User successfully Updated" });

    })


})

user.use(function (req, res, next) {

    //console.log("after action with error",req);

    res.status(404).json({ message: "Page not found" });
    //next();

});

module.exports = user;
