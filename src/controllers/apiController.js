'use strict';
var express    = require('express');
var api=express.Router();
var methodOverride = require('method-override');

var user =require("./userController");

api.use((req,res,next)=>{

        console.log("Before Action..");
        next();

})

api.use('/user',user);


api.use((req,res,next)=>{
    
            console.log("after Action..");
    
            res.status(404).json({ message: "Page not found" });
    
            //next();
    
    })

module.exports=api;