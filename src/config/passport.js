var BearerStrategy = require('passport-http-bearer').Strategy;
var LocalStrategy = require('passport-local').Strategy;

var User = require("../modules/api/modals/userModal").User;
var Token = require("../modules/api/modals/userModal").Token;

module.exports = function (passport) {

	console.log("Passport Config init...");
	passport.serializeUser(function (user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function (id, done) {
		User.findById(id, function (err, user) {
			done(err, user);
		});
	});

	passport.use('local-login',new LocalStrategy(function (username, password, done) {

		User.findOne({ username: username }, function (err, user) {
		
			if (err) { return done(err); }

			if (!user) {
				return done(null, false, { message: 'Incorrect username.' });
			}
			if (!user.validPassword(password)) {
				return done(null, false, { message: 'Incorrect password.' });
			}
			return done(null, user);

		})

	}))

	passport.use(new BearerStrategy({},
		function (token, done) {
			Token.findOne({ value: token }).populate('user').exec(function (err, token) {
				if (!token)
					return done(null, false);
				return done(null, token.user);
			})
		}));
}