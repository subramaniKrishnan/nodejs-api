
var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var passport = require("passport");
var flash = require('connect-flash');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);




app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
var ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || 'localhost';
var router = express.Router();

app.use(express.static(__dirname + '/public'));

app.set('view engine', 'ejs');

//modals

var User = require('./src/modules/api/modals/userModal').User;
var Token = require('./src/modules/api/modals/userModal').Token;
//controllers



//var userController =require('./src/controllers/userController');
var mongoose = require('mongoose');
mongoose.connect('mongodb://demo:fz1UNHFWMGHXP3Su@cluster0-shard-00-00-culax.mongodb.net:27017,cluster0-shard-00-01-culax.mongodb.net:27017,cluster0-shard-00-02-culax.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin'); // connect to our database
var apiController = require('./src/modules/api/api');

app.use(session({
        secret: 'storesessionobj',
        store: new MongoStore({ mongooseConnection: mongoose.connection, ttl: 2 * 24 * 60 * 60 }),
        resave: true,
        saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session())
app.use(flash()); // use connect-flash for flash messages stored in session
require("./src/config/passport")(passport);


app.use('/api', apiController);

app.get('/', function (req, res) {
        res.render("index", { message: "Its Me" });
})

app.get('/auth', function (req, res) {

        console.log("flashMessage", req.flash("error"))

        res.render("login", { message: "Its Me" });

})

app.post("/auth", passport.authenticate("local-login", {
        successRedirect: '/',
        failureRedirect: '/auth',
        failureFlash: true
}))

app.post("/register", function (req, res) {
        const data = req.body;


        User.findOne({ email: data.email }, function (err, user) {

                if (!user) {
                        let muser = new User(data);
                        muser.password = muser.generateHash(muser.password);
                        muser.save(function (err, user) {

                                if (err) {
                                        res.json({ "error": err });
                                }

                                if (user) {


                                        user.generateToken();
                                        
                                        res.json({ user: user })
                                }

                               

                        });


                }else{

                        res.json({ "error": "This is user already " });

                }


        });
})

// START THE SERVER
// =============================================================================
app.listen(port, ip);
console.log('Server running on http://%s:%s', ip, port);
module.exports = app;
// /https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4
//http://mongoosejs.com/docs/guide.html#validateBeforeSave