var codeProtect = require('code-protect');
var option = {
    sourceDir: './api',
    destinationDir: './new-api',
    uglify:true,
    debug: true
};
 
codeProtect(option, function (err, data) {
    if (err) {
        throw err;
    } else {
        console.log(data);
    }
});